from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Account

# Create your tests here.


class AuthTest(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/authentication/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_template(self):
        response = Client().get('/authentication/login/')
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_login_using_login_func(self):
        found = resolve('/authentication/login/')
        self.assertEqual(found.func, views.login)

    def test_register_url_is_exist(self):
        response = Client().get('/authentication/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_register_template(self):
        response = Client().get('/authentication/register/')
        self.assertTemplateUsed(response, 'authentication/register.html')

    def test_login_using_login_func(self):
        found = resolve('/authentication/register/')
        self.assertEqual(found.func, views.register)

    def test_model_can_create_account(self):
        new_account = Account.objects.create_user(
            nama_depan="TestDepan",
            nama_belakang="TestBelakang",
            email="test@gmail.com",
            username="test_user",
            password="password",
        )
        count = Account.objects.count()
        self.assertEqual(1, count)

    def test_can_register(self):
        data = {
            "nama_depan": "TestDepan",
            "nama_belakang": "TestBelakang",
            "email": "test@gmail.com",
            "username": "test_user",
            "password": "password",
            "confirm_password": "password",
            "title": "UI/UX Designer",
            "biography": "Saya ganteng"
        }

        response = Client().post('/authentication/register/', data=data)

        count = Account.objects.count()
        self.assertEqual(1, count)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/dashboard/')

    def test_can_login(self):
        new_account = Account.objects.create_user(
            nama_depan="TestDepan",
            nama_belakang="TestBelakang",
            email="test@gmail.com",
            username="test_user",
            password="password",
        )

        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        response = Client().post("/authentication/login/", data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/dashboard/')
