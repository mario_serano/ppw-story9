from django.shortcuts import render, redirect

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login as django_login,
    logout as django_logout
)

from .forms import (
    UserRegisterForm,
    UserLoginForm
)

# Create your views here.


def login(request):
    if request.user.is_authenticated:
        return redirect("/dashboard/")

    # Redirect user, hidden input, next
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(email=email, password=password)
        django_login(request, user)
        if next:
            return redirect(next)
        return redirect('/dashboard/')

    context = {
        'form': form,
    }

    return render(request, "authentication/login.html", context)


def register(request):
    if request.user.is_authenticated:
        return redirect("/dashboard/")

    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)  # False means don't save it yet
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(email=user.email, password=password)
        django_login(request, user)
        if next:
            return redirect(next)
        return redirect('/dashboard/')

    context = {
        'form': form,
    }
    return render(request, "authentication/register.html", context)


def logout(request):
    django_logout(request)
    return redirect('/')
