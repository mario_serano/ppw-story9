from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import (
    get_user_model,
    login
)
from authentication.models import Account
from . import views

# Create your tests here.


class DashboardTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_dashboard_url_is_exist_if_user_login(self):
        new_account = Account.objects.create_user(
            nama_depan="TestDepan",
            nama_belakang="TestBelakang",
            email="test@gmail.com",
            username="test_user",
            password="password",
        )
        self.client.login(email="test@gmail.com", password="password")
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_using_index_template(self):
        new_account = Account.objects.create_user(
            nama_depan="TestDepan",
            nama_belakang="TestBelakang",
            email="test@gmail.com",
            username="test_user",
            password="password",
        )
        self.client.login(email="test@gmail.com", password="password")
        response = self.client.get('/dashboard/')
        self.assertTemplateUsed(response, 'dashboard/index.html')

    def test_dashboard_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, views.index)
